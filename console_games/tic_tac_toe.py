
from random import randrange

WELCOME = """
          Welcome to Tic-tac-toe!
          [1] Start the game
          [0] Quit
          """
X = "X"
O = "O"
TIE = "TIE"
EMPTY_FIELD = " "
BEST_MOVES = (4, 0, 2, 6, 8, 1, 3, 5, 7)
WAYS_TO_WIN = ((0, 1, 2), (3, 4, 5),
               (6, 7, 8), (0, 3, 6),
               (1, 4, 7), (2, 5, 8),
               (0, 4, 8), (2, 4, 6))


def menu():
    print(WELCOME)
    user_select = None
    while user_select != "0":
        user_select = input("Select: ")
        if user_select == "1":
            break
        elif user_select == "0":
            exit()


def draw_tokens():
    token = randrange(2)
    if token == 0:
        user_token = X
        cpu_token = O
    else:
        user_token = O
        cpu_token = X
    return user_token, cpu_token


def create_board():
    board = []
    for i in range(0, 9):
        board.append(EMPTY_FIELD)
    return board


def display_board(board):
    print("\t", board[0], "|", board[1], "|", board[2])
    print("\t-----------")
    print("\t", board[3], "|", board[4], "|", board[5])
    print("\t-----------")
    print("\t", board[6], "|", board[7], "|", board[8])


def legal_moves(board):
    moves = []
    for i in range(9):
        if board[i] == EMPTY_FIELD:
            moves.append(i)
    return moves


def user_move(board):
    legal = legal_moves(board)
    move = None
    while move not in legal:
        move = int(input("Select field: "))
        if move not in legal:
            print("This field is occupied. Choose other.")
    return move


def cpu_move(board):
    board = board[:]
    for move in BEST_MOVES:
        if move in legal_moves(board):
            print("\nComputer picks the field")
            return move


def next_turn(turn):
    if turn == X:
        return O
    else:
        return X


def win_conditions(board):
    for i in WAYS_TO_WIN:
        if board[i[0]] == board[i[1]] == board[i[2]] != EMPTY_FIELD:
            winner = board[i[0]]
            return winner
    if EMPTY_FIELD not in board:
        return TIE
    else:
        return None


def end_game(winner, user_token, cpu_token):
    if winner == user_token:
        print("You win!")
    elif winner == cpu_token:
        print("Computer wins!")
    elif winner == TIE:
        print("TIE!")


def main():
    menu()
    board = create_board()
    user_token, cpu_token = draw_tokens()
    turn = X
    display_board(board)

    while not win_conditions(board):
        if turn == user_token:
            move = user_move(board)
            board[move] = user_token
        else:
            move = cpu_move(board)
            board[move] = cpu_token
        turn = next_turn(turn)
        display_board(board)

    winner = win_conditions(board)
    end_game(winner, user_token, cpu_token)

main()

input("Press ENTER to exit")